﻿using System;
using System.Windows.Forms;

namespace Playfair_Cipher
{
    public partial class PlayFairMainForm : Form
    {
        public PlayFairMainForm()
        {
            InitializeComponent();
            Cipher.CreateCryptingMatrix(string.Empty);
            DisplayCryptingMatrix(Cipher.CryptingMatrix);
        }

        private void DisplayCryptingMatrix(string[][] cryptingMatrix)
        {
            alphabetDGView.Rows.Clear();
            alphabetDGView.ColumnCount = cryptingMatrix.Length;

            for (int i = 0; i < cryptingMatrix.GetLength(0); i++)
            {
                alphabetDGView.Rows.Add(cryptingMatrix[i]);
            }
        }

        private void keyTextBox_TextChanged(object sender, EventArgs e)
        {
            formattedKeyTextBox.Text = Cipher.FilterKey(keyTextBox.Text);
            Cipher.CreateCryptingMatrix(Cipher.CryptingKey);
            DisplayCryptingMatrix(Cipher.CryptingMatrix);
        }

        private bool IsKeyCorrect()
        {
            if (keyTextBox.Text.Length == 0)
            {
                MessageBox.Show("Zadajte prosím kľúč", "Chýbajúci kľúč",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (Cipher.CryptingKey == null || Cipher.CryptingKey == "")
            {
                MessageBox.Show("Zadaný kľúč neobsahuje platné znaky abecedy.",
                    "Neplatný kľúč", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void encryptButton_Click(object sender, EventArgs e)
        {
            if (IsKeyCorrect())
            {
                string originalMessage = originalTextRTBox.Text;
                string filteredMessage = Cipher.FilterText(originalMessage);

                if (originalMessage.Length == 0)
                {
                    MessageBox.Show("Zadajte prosím správu k zašifrovaniu.",
                        "Chýbajúca správa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (filteredMessage.Length == 0)
                {
                    MessageBox.Show("Zadaná správa neobsahuje platné znaky abecedy.",
                        "Neplatná správa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string original = originalTextRTBox.Text;
                    string filtered = Cipher.FilterText(original);
                    string digramsFiltered = Cipher.FilterDigrams(filtered);

                    filteredTextRTBox.Text = Cipher.FormatOutput(digramsFiltered, 2);
                    encryptedRTBox.Text = Cipher.FormatOutput(Cipher.EncryptText(digramsFiltered), 5);
                }
            }
        }

        private void copyButton_Click(object sender, EventArgs e)
        {
            encryptedRTBox.Focus();
            encryptedRTBox.SelectAll();
            encryptedRTBox.Copy();
        }

        private void pasteButton_Click(object sender, EventArgs e)
        {
            textToDecryptRTBox.Focus();
            textToDecryptRTBox.Clear();
            textToDecryptRTBox.Paste();
        }

        private void decryptButton_Click(object sender, EventArgs e)
        {
            if (IsKeyCorrect())
            {
                string encryptedText = textToDecryptRTBox.Text.Replace(" ", string.Empty);

                if (encryptedText.Length == 0)
                {
                    MessageBox.Show("Zadajte prosím správu k dešifrovaniu.",
                        "Chýbajúca správa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string decryptedText = Cipher.DecryptText(encryptedText);
                    string digramDefiltered = Cipher.DefilterDigrams(decryptedText);
                    string defilteredText = Cipher.DefilterText(digramDefiltered);

                    decryptedTextRTBox.Text = defilteredText;
                }
            }
        }
    }
}
