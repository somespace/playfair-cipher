﻿using MMLib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Playfair_Cipher
{
    class Cipher
    {
        public static string CryptingKey { get; private set; } = null;

        private static readonly string[] alphabet =
        { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "Z" };
        public static string[][] CryptingMatrix { get; private set; } = new string[5][];

        public static string[] nonAlphabetChars =
        {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            ".", ",", ":", ";", "?", "!", "\"", "\'", "\\", "/",
            "~", "@", "#", "%", "^", "&", "*", "(", ")", "_", "-",
            "+", "=", "[", "]", "{", "}", " "
        };

        public static string[] nonAlphabetCharCodes =
        {
            "IVPTZBHRGH", "YZRTLPVCVI", "VMEFGPZYHN", "IOYTNRZBTO", "RYVZXLIPLB",
            "PMVZCVNHPW", "POISPUFMRI", "XVMCZTCPUI", "RVTRVBGNPO", "LNMAVYHLPI",
            "BGEHLDMBRN", "OVTRYIOENI", "HRHKFRIHFS", "JOGNFDESIC", "NRDOUTEGFJ",
            "OLCBGEFSTO", "POZNBRJZLC", "HVZPTRCKVX", "NIOTBEORTS", "OMTNRZCKVD",
            "CSLJERBECV", "LIOERNTOVZ", "BOEJRNBERP", "NCVCIZTNES", "MIOENZCVZT",
            "ZYBNMCTOVI", "IOVERNKLOI", "OPRVPKYCEB", "ZDTLONTROC", "PMCVTKSLKM",
            "ZOIVHNODPG", "MOUEIYMIEI", "ZNCVBGBVEM", "CIRVOIPBMH", "NESDLSVRBH",
            "OUFGFMONCV", "MKEIRTNHIS", "HMLRZBRTKC"
        };

        public static string FilterKey(string key)
        {
            string formattedKey = key.RemoveDiacritics().ToUpper().Replace("W", "V");
            string filteredKey = "";

            foreach (char c in formattedKey)
            {
                string keyChar = c.ToString();
                if (!(filteredKey.Contains(keyChar)) && alphabet.Contains(keyChar))
                    filteredKey += keyChar;
            }

            CryptingKey = filteredKey;
            return filteredKey;
        }

        private static string FilterAlphabetByKey(string filteredKey)
        {
            string filteredAlphabet = "";

            foreach (string s in alphabet)
            {
                if (!filteredKey.Contains(s))
                {
                    filteredAlphabet += s;
                }
            }

            return filteredAlphabet;
        }

        public static void CreateCryptingMatrix(string filteredKey)
        {
            string cryptingAlphabet = filteredKey + FilterAlphabetByKey(filteredKey);
            int charPos = 0;

            for (int i = 0; i < CryptingMatrix.GetLength(0); i++)
            {
                CryptingMatrix[i] = new string[5];

                for (int j = 0; j < CryptingMatrix[i].Length; j++)
                {
                    CryptingMatrix[i][j] = cryptingAlphabet[charPos++].ToString();
                }
            }
        }

        public static string FilterDigrams(string filtered)
        {
            string digramFiltered = "";

            for (int i = 0; i < filtered.Length; i++)
            {
                if (i == filtered.Length - 1)
                {
                    if (filtered[i] != 'X')
                    {
                        digramFiltered += filtered[i++] + "X";
                    }
                    else
                    {
                        digramFiltered += filtered[i++] + "Q";
                    }
                    continue;
                }

                string second = filtered[i + 1].ToString();
                string first = filtered[i].ToString();

                if (first != second)
                {
                    digramFiltered += first + second;
                    i++;
                }
                else if (first == "X")
                {
                    digramFiltered += "XQ";
                }
                else
                {
                    digramFiltered += filtered[i] + "X";
                }
            }

            return digramFiltered;
        }

        public static string DefilterDigrams(string decryptedText)
        {
            string digramDefiltered = "";

            for (int i = 0; i < decryptedText.Length; i++)
            {
                if (i == decryptedText.Length - 2)
                {
                    string actual = decryptedText[i].ToString();
                    string last = decryptedText[i + 1].ToString();

                    if (actual + last != "XQ" && last != "X")
                    {
                        digramDefiltered += actual + last;
                    }
                    else
                    {
                        digramDefiltered += actual;
                    }
                    i += 2;
                    continue;
                }

                string first = decryptedText[i].ToString();
                string second = decryptedText[i + 1].ToString();
                string third = decryptedText[i + 2].ToString();

                if ((second == "X" && first == third) || first + second == "XQ")
                {
                    digramDefiltered += first;
                }
                else
                {
                    digramDefiltered += first + second;
                }
                i++;
            }
            return digramDefiltered;
        }
                    
        public static string FilterText(string text)
        {
            string diaremoved = text.Replace('W', 'V').RemoveDiacritics();
            string filtered = "";

            for (int i = 0; i < diaremoved.Length; i++)
            {
                if (alphabet.Contains(diaremoved[i].ToString().ToUpper()))
                {
                    filtered += diaremoved[i].ToString().ToUpper();
                }
                else if (nonAlphabetChars.Contains(diaremoved[i].ToString()))
                {
                    int specialCharPosition = Array.IndexOf(nonAlphabetChars, diaremoved[i].ToString());
                    filtered += nonAlphabetCharCodes[specialCharPosition];
                }
            }

            return filtered;
        }

        public static string DefilterText(string filtered)
        {
            StringBuilder defilteredText = new StringBuilder(filtered);

            for (int i = 0; i < nonAlphabetCharCodes.Length; i++)
            {
                if (filtered.Contains(nonAlphabetCharCodes[i]))
                {
                    defilteredText.Replace(nonAlphabetCharCodes[i], nonAlphabetChars[i]);
                }
            }
            return defilteredText.ToString();
        }

        private static int[] GetCryptMatrixPosition(string character)
        {
            int[] charMatrixIndex = null;

            for (int i = 0; i < CryptingMatrix.Length; i++)
            {
                for (int j = 0; j < CryptingMatrix.Length; j++)
                {
                    if (CryptingMatrix[i][j] == character)
                    {
                        charMatrixIndex = new int[] { i, j };
                    }
                }
            }

            return charMatrixIndex;
        }

        public static string EncryptText(string digramFiltered)
        {
            string encryptedText = "";
            string[] digramsArray = FormatOutput(digramFiltered, 2).Split(' ');

            foreach (string digram in digramsArray)
            {
                int matrixSize = CryptingMatrix.Length;

                string firstChar = digram[0].ToString();
                string secondChar = digram[1].ToString();

                int[] firstPos = GetCryptMatrixPosition(firstChar);
                int firstRowIndex = firstPos[0];
                int firstColIndex = firstPos[1];

                int[] secondPos = GetCryptMatrixPosition(secondChar);
                int secondRowIndex = secondPos[0];
                int secondColIndex = secondPos[1];

                if (firstRowIndex == secondRowIndex)
                {
                    encryptedText += CryptingMatrix[firstRowIndex][(firstColIndex + 1) % matrixSize];
                    encryptedText += CryptingMatrix[secondRowIndex][(secondColIndex + 1) % matrixSize];
                }
                else if (firstColIndex == secondColIndex)
                {
                    encryptedText += CryptingMatrix[(firstRowIndex + 1) % matrixSize][firstColIndex];
                    encryptedText += CryptingMatrix[(secondRowIndex + 1) % matrixSize][secondColIndex];
                }
                else
                {
                    encryptedText += CryptingMatrix[firstRowIndex][secondColIndex];
                    encryptedText += CryptingMatrix[secondRowIndex][firstColIndex];
                }
            }

            return encryptedText;
        }

        public static string FormatOutput(string text, int chunkSize)
        {
            string formatted = "";

            for (int i = 0; i < text.Length; i++)
            {
                formatted += text[i];
                if (((i + 1) % chunkSize == 0) && (i < text.Length - 1))
                {
                    formatted += " ";
                }
            }

            return formatted;
        }

        public static string DecryptText(string encryptedText)
        {
            string decryptedText = "";
            string[] digramsArray = FormatOutput(encryptedText, 2).Split(' ');

            foreach (string digram in digramsArray)
            {
                int matrixSize = CryptingMatrix.Length;

                string firstChar = digram[0].ToString();
                string secondChar = digram[1].ToString();

                int[] firstPos = GetCryptMatrixPosition(firstChar);
                int firstRowIndex = firstPos[0];
                int firstColIndex = firstPos[1];

                int[] secondPos = GetCryptMatrixPosition(secondChar);
                int secondRowIndex = secondPos[0];
                int secondColIndex = secondPos[1];

                if (firstRowIndex == secondRowIndex)
                {
                    firstColIndex = (firstColIndex == 0) ? matrixSize : firstColIndex;
                    secondColIndex = (secondColIndex == 0) ? matrixSize : secondColIndex;
                    
                    decryptedText += CryptingMatrix[firstRowIndex][firstColIndex - 1];
                    decryptedText += CryptingMatrix[secondRowIndex][secondColIndex - 1];
                }
                else if (firstColIndex == secondColIndex)
                {
                    firstRowIndex = (firstRowIndex == 0) ? matrixSize : firstRowIndex;
                    secondRowIndex = (secondRowIndex == 0) ? matrixSize : secondRowIndex;

                    decryptedText += CryptingMatrix[firstRowIndex - 1][firstColIndex];
                    decryptedText += CryptingMatrix[secondRowIndex - 1][secondColIndex];
                }
                else
                {
                    decryptedText += CryptingMatrix[firstRowIndex][secondColIndex];
                    decryptedText += CryptingMatrix[secondRowIndex][firstColIndex];
                }
            }

            return decryptedText;
        }
    }
}
