﻿namespace Playfair_Cipher
{
    partial class PlayFairMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.Zašifrovanie = new System.Windows.Forms.TabPage();
            this.copyButton = new System.Windows.Forms.Button();
            this.encryptButton = new System.Windows.Forms.Button();
            this.encryptedTextLabel = new System.Windows.Forms.Label();
            this.encryptedRTBox = new System.Windows.Forms.RichTextBox();
            this.FilteredTextLabel = new System.Windows.Forms.Label();
            this.originalTextLabel = new System.Windows.Forms.Label();
            this.filteredTextRTBox = new System.Windows.Forms.RichTextBox();
            this.originalTextRTBox = new System.Windows.Forms.RichTextBox();
            this.Dešifrovanie = new System.Windows.Forms.TabPage();
            this.decryptButton = new System.Windows.Forms.Button();
            this.pasteButton = new System.Windows.Forms.Button();
            this.decryptedTextLabel = new System.Windows.Forms.Label();
            this.decryptedTextRTBox = new System.Windows.Forms.RichTextBox();
            this.textToDecryptLabel = new System.Windows.Forms.Label();
            this.textToDecryptRTBox = new System.Windows.Forms.RichTextBox();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.keyTextLabel = new System.Windows.Forms.Label();
            this.formattedKeyTextBox = new System.Windows.Forms.TextBox();
            this.formattedKeyLabel = new System.Windows.Forms.Label();
            this.alphabetDGView = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MainTabControl.SuspendLayout();
            this.Zašifrovanie.SuspendLayout();
            this.Dešifrovanie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alphabetDGView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.Zašifrovanie);
            this.MainTabControl.Controls.Add(this.Dešifrovanie);
            this.MainTabControl.Location = new System.Drawing.Point(12, 274);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(842, 411);
            this.MainTabControl.TabIndex = 0;
            // 
            // Zašifrovanie
            // 
            this.Zašifrovanie.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Zašifrovanie.Controls.Add(this.copyButton);
            this.Zašifrovanie.Controls.Add(this.encryptButton);
            this.Zašifrovanie.Controls.Add(this.encryptedTextLabel);
            this.Zašifrovanie.Controls.Add(this.encryptedRTBox);
            this.Zašifrovanie.Controls.Add(this.FilteredTextLabel);
            this.Zašifrovanie.Controls.Add(this.originalTextLabel);
            this.Zašifrovanie.Controls.Add(this.filteredTextRTBox);
            this.Zašifrovanie.Controls.Add(this.originalTextRTBox);
            this.Zašifrovanie.Location = new System.Drawing.Point(4, 29);
            this.Zašifrovanie.Name = "Zašifrovanie";
            this.Zašifrovanie.Padding = new System.Windows.Forms.Padding(3);
            this.Zašifrovanie.Size = new System.Drawing.Size(834, 378);
            this.Zašifrovanie.TabIndex = 0;
            this.Zašifrovanie.Text = "Zašifrovanie";
            // 
            // copyButton
            // 
            this.copyButton.Location = new System.Drawing.Point(429, 313);
            this.copyButton.Name = "copyButton";
            this.copyButton.Size = new System.Drawing.Size(176, 40);
            this.copyButton.TabIndex = 7;
            this.copyButton.Text = "Kopíruj";
            this.copyButton.UseVisualStyleBackColor = true;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // encryptButton
            // 
            this.encryptButton.Location = new System.Drawing.Point(17, 313);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(176, 40);
            this.encryptButton.TabIndex = 6;
            this.encryptButton.Text = "Zašifruj";
            this.encryptButton.UseVisualStyleBackColor = true;
            this.encryptButton.Click += new System.EventHandler(this.encryptButton_Click);
            // 
            // encryptedTextLabel
            // 
            this.encryptedTextLabel.AutoSize = true;
            this.encryptedTextLabel.Location = new System.Drawing.Point(425, 156);
            this.encryptedTextLabel.Name = "encryptedTextLabel";
            this.encryptedTextLabel.Size = new System.Drawing.Size(124, 20);
            this.encryptedTextLabel.TabIndex = 5;
            this.encryptedTextLabel.Text = "Zašifrovaný text:";
            // 
            // encryptedRTBox
            // 
            this.encryptedRTBox.Location = new System.Drawing.Point(429, 179);
            this.encryptedRTBox.Name = "encryptedRTBox";
            this.encryptedRTBox.Size = new System.Drawing.Size(385, 113);
            this.encryptedRTBox.TabIndex = 4;
            this.encryptedRTBox.Text = "";
            // 
            // FilteredTextLabel
            // 
            this.FilteredTextLabel.AutoSize = true;
            this.FilteredTextLabel.Location = new System.Drawing.Point(425, 16);
            this.FilteredTextLabel.Name = "FilteredTextLabel";
            this.FilteredTextLabel.Size = new System.Drawing.Size(110, 20);
            this.FilteredTextLabel.TabIndex = 3;
            this.FilteredTextLabel.Text = "Filtrovaný text:";
            // 
            // originalTextLabel
            // 
            this.originalTextLabel.AutoSize = true;
            this.originalTextLabel.Location = new System.Drawing.Point(13, 16);
            this.originalTextLabel.Name = "originalTextLabel";
            this.originalTextLabel.Size = new System.Drawing.Size(103, 20);
            this.originalTextLabel.TabIndex = 2;
            this.originalTextLabel.Text = "Pôvodný text:";
            // 
            // filteredTextRTBox
            // 
            this.filteredTextRTBox.Location = new System.Drawing.Point(429, 41);
            this.filteredTextRTBox.Name = "filteredTextRTBox";
            this.filteredTextRTBox.Size = new System.Drawing.Size(385, 102);
            this.filteredTextRTBox.TabIndex = 1;
            this.filteredTextRTBox.Text = "";
            // 
            // originalTextRTBox
            // 
            this.originalTextRTBox.Location = new System.Drawing.Point(17, 41);
            this.originalTextRTBox.Name = "originalTextRTBox";
            this.originalTextRTBox.Size = new System.Drawing.Size(382, 251);
            this.originalTextRTBox.TabIndex = 0;
            this.originalTextRTBox.Text = "";
            // 
            // Dešifrovanie
            // 
            this.Dešifrovanie.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Dešifrovanie.Controls.Add(this.decryptButton);
            this.Dešifrovanie.Controls.Add(this.pasteButton);
            this.Dešifrovanie.Controls.Add(this.decryptedTextLabel);
            this.Dešifrovanie.Controls.Add(this.decryptedTextRTBox);
            this.Dešifrovanie.Controls.Add(this.textToDecryptLabel);
            this.Dešifrovanie.Controls.Add(this.textToDecryptRTBox);
            this.Dešifrovanie.Location = new System.Drawing.Point(4, 29);
            this.Dešifrovanie.Name = "Dešifrovanie";
            this.Dešifrovanie.Padding = new System.Windows.Forms.Padding(3);
            this.Dešifrovanie.Size = new System.Drawing.Size(834, 378);
            this.Dešifrovanie.TabIndex = 1;
            this.Dešifrovanie.Text = "Dešifrovanie";
            // 
            // decryptButton
            // 
            this.decryptButton.Location = new System.Drawing.Point(429, 312);
            this.decryptButton.Name = "decryptButton";
            this.decryptButton.Size = new System.Drawing.Size(176, 40);
            this.decryptButton.TabIndex = 10;
            this.decryptButton.Text = "Dešifruj";
            this.decryptButton.UseVisualStyleBackColor = true;
            this.decryptButton.Click += new System.EventHandler(this.decryptButton_Click);
            // 
            // pasteButton
            // 
            this.pasteButton.Location = new System.Drawing.Point(17, 312);
            this.pasteButton.Name = "pasteButton";
            this.pasteButton.Size = new System.Drawing.Size(176, 40);
            this.pasteButton.TabIndex = 9;
            this.pasteButton.Text = "Vlož";
            this.pasteButton.UseVisualStyleBackColor = true;
            this.pasteButton.Click += new System.EventHandler(this.pasteButton_Click);
            // 
            // decryptedTextLabel
            // 
            this.decryptedTextLabel.AutoSize = true;
            this.decryptedTextLabel.Location = new System.Drawing.Point(425, 16);
            this.decryptedTextLabel.Name = "decryptedTextLabel";
            this.decryptedTextLabel.Size = new System.Drawing.Size(126, 20);
            this.decryptedTextLabel.TabIndex = 6;
            this.decryptedTextLabel.Text = "Dešifrovaný text:";
            // 
            // decryptedTextRTBox
            // 
            this.decryptedTextRTBox.Location = new System.Drawing.Point(429, 42);
            this.decryptedTextRTBox.Name = "decryptedTextRTBox";
            this.decryptedTextRTBox.Size = new System.Drawing.Size(386, 249);
            this.decryptedTextRTBox.TabIndex = 5;
            this.decryptedTextRTBox.Text = "";
            // 
            // textToDecryptLabel
            // 
            this.textToDecryptLabel.AutoSize = true;
            this.textToDecryptLabel.Location = new System.Drawing.Point(13, 16);
            this.textToDecryptLabel.Name = "textToDecryptLabel";
            this.textToDecryptLabel.Size = new System.Drawing.Size(124, 20);
            this.textToDecryptLabel.TabIndex = 4;
            this.textToDecryptLabel.Text = "Zašifrovaný text:";
            // 
            // textToDecryptRTBox
            // 
            this.textToDecryptRTBox.Location = new System.Drawing.Point(17, 42);
            this.textToDecryptRTBox.Name = "textToDecryptRTBox";
            this.textToDecryptRTBox.Size = new System.Drawing.Size(382, 249);
            this.textToDecryptRTBox.TabIndex = 3;
            this.textToDecryptRTBox.Text = "";
            // 
            // keyTextBox
            // 
            this.keyTextBox.Location = new System.Drawing.Point(21, 60);
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.Size = new System.Drawing.Size(382, 26);
            this.keyTextBox.TabIndex = 1;
            this.keyTextBox.TextChanged += new System.EventHandler(this.keyTextBox_TextChanged);
            // 
            // keyTextLabel
            // 
            this.keyTextLabel.AutoSize = true;
            this.keyTextLabel.Location = new System.Drawing.Point(17, 37);
            this.keyTextLabel.Name = "keyTextLabel";
            this.keyTextLabel.Size = new System.Drawing.Size(45, 20);
            this.keyTextLabel.TabIndex = 2;
            this.keyTextLabel.Text = "Kľúč:";
            // 
            // formattedKeyTextBox
            // 
            this.formattedKeyTextBox.Location = new System.Drawing.Point(21, 138);
            this.formattedKeyTextBox.Name = "formattedKeyTextBox";
            this.formattedKeyTextBox.Size = new System.Drawing.Size(382, 26);
            this.formattedKeyTextBox.TabIndex = 7;
            // 
            // formattedKeyLabel
            // 
            this.formattedKeyLabel.AutoSize = true;
            this.formattedKeyLabel.Location = new System.Drawing.Point(17, 115);
            this.formattedKeyLabel.Name = "formattedKeyLabel";
            this.formattedKeyLabel.Size = new System.Drawing.Size(139, 20);
            this.formattedKeyLabel.TabIndex = 6;
            this.formattedKeyLabel.Text = "Formátovaný kľúč:";
            // 
            // alphabetDGView
            // 
            this.alphabetDGView.AllowUserToAddRows = false;
            this.alphabetDGView.AllowUserToDeleteRows = false;
            this.alphabetDGView.AllowUserToResizeColumns = false;
            this.alphabetDGView.AllowUserToResizeRows = false;
            this.alphabetDGView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.alphabetDGView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.alphabetDGView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.alphabetDGView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.alphabetDGView.ColumnHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.alphabetDGView.DefaultCellStyle = dataGridViewCellStyle5;
            this.alphabetDGView.Location = new System.Drawing.Point(447, 31);
            this.alphabetDGView.Name = "alphabetDGView";
            this.alphabetDGView.RowHeadersVisible = false;
            this.alphabetDGView.RowHeadersWidth = 62;
            this.alphabetDGView.RowTemplate.Height = 28;
            this.alphabetDGView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.alphabetDGView.Size = new System.Drawing.Size(401, 217);
            this.alphabetDGView.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.keyTextLabel);
            this.panel1.Controls.Add(this.formattedKeyTextBox);
            this.panel1.Controls.Add(this.formattedKeyLabel);
            this.panel1.Controls.Add(this.keyTextBox);
            this.panel1.Location = new System.Drawing.Point(12, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(435, 217);
            this.panel1.TabIndex = 10;
            // 
            // PlayFairMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(860, 697);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.alphabetDGView);
            this.Controls.Add(this.MainTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PlayFairMainForm";
            this.Text = "Playfairova šifra";
            this.MainTabControl.ResumeLayout(false);
            this.Zašifrovanie.ResumeLayout(false);
            this.Zašifrovanie.PerformLayout();
            this.Dešifrovanie.ResumeLayout(false);
            this.Dešifrovanie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alphabetDGView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage Zašifrovanie;
        private System.Windows.Forms.TabPage Dešifrovanie;
        private System.Windows.Forms.RichTextBox originalTextRTBox;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.Label keyTextLabel;
        private System.Windows.Forms.Label originalTextLabel;
        private System.Windows.Forms.RichTextBox filteredTextRTBox;
        private System.Windows.Forms.Label FilteredTextLabel;
        private System.Windows.Forms.RichTextBox encryptedRTBox;
        private System.Windows.Forms.Label encryptedTextLabel;
        private System.Windows.Forms.Label decryptedTextLabel;
        private System.Windows.Forms.RichTextBox decryptedTextRTBox;
        private System.Windows.Forms.Label textToDecryptLabel;
        private System.Windows.Forms.RichTextBox textToDecryptRTBox;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.Label formattedKeyLabel;
        private System.Windows.Forms.Button decryptButton;
        private System.Windows.Forms.Button pasteButton;
        private System.Windows.Forms.TextBox formattedKeyTextBox;
        private System.Windows.Forms.DataGridView alphabetDGView;
        private System.Windows.Forms.Panel panel1;
    }
}

